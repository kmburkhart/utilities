#!/bin/bash

# Takes arguments passed to script and routs them accordingly
function dispatch() {
  while :
    do
      case $1 in

        -h | --help | \?)
          # Call to _print_usage()
          # Exits with zero; this is not an error, the user asked for help.
          _print_usage;
          exit 0;
          shift ;;

        -v | --verbose)
          VERBOSE=1
          shift ;;

        --)
          # End of all options
          shift
          break ;;

        -*)
          _print_error $@
          shift ;;

        *)
          # No more options to process.
          break ;;
    esac
  done

  ## Defensive programming towards usage.
  #if [ -z "$1" ]; then
  #  _print_error $1
  #fi
  #
  #if [ -z "$2" ]; then
  #  _print_error $2
  #fi
  #
  ## Set verbosity if flag is set.
  #if [ "$VERBOSE" == 1 ]; then
  #  set -vx;
  #fi

  # Resets log file.
  > ~/init-puppet-server.log;

  main
}

function main() {
  yumUpdates
  exit 0
}

function yumUpdates() {
  echo -e "\nUpdating yum.\n"
  yum update -y
  echo -e "\nInstalling all available upgrades.\n"
  yum upgrade -y
}

# Prints text in color
function _colorize() {

  local color=${1}
  shift
  local text="${@}"

  case ${color} in
    red    ) tput setaf 1 ; tput bold ;;
    green  ) tput setaf 2 ; tput bold ;;
    yellow ) tput setaf 3 ; tput bold ;;
    blue   ) tput setaf 4 ; tput bold ;;
    grey   ) tput setaf 7 ; tput bold ; tput setaf 0 ;;
  esac

  echo -en "${text}"
  tput sgr0
}

# Prints the command name without path.
function _cmd(){
  echo $(basename $0);
}

# Prints standard error message when an unknown option is encountered.
function _print_error() {
  echo -e $(_colorize red "[error]") "Invalid option '$1' received."
  echo "Try '$(_cmd) --help' for more information.";
  exit 1;
}

function _print_usage() {
  cat <<EOF
        Usage: $0 (options)

        Options:
                -h   --help      Display this help system.
                -v   --verbose   Run the script with verbose mode enabled.

        Examples:

        * Run the script and display this help system.
          $0 --help

EOF
}

# Call dispatch using user specified options.
dispatch $@;